class Employee{
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
        this.extra = 3
    }
    get FullInfo(){
        return `${this.name} ${this.age} ${this.salary}`
    }
    set FullInfo(value){
        [this.name, this.age, this.salary] = value
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
    get FullInfoUser() {
        return `${this.name} ${this.age} ${this.salary * this.extra} ${this.lang}`
    }
    set FullInfoUser(value){
        [this.name, this.age, this.salary, this.lang] = value
    }
}

const programmerJane = new Programmer('Jane', '21', 6000, 'english, german, ukraine')
const programmerNick = new Programmer('Nick', '25', 7000, 'spanish, italian, english')
console.log(programmerJane.FullInfoUser)
console.log(programmerNick.FullInfoUser)