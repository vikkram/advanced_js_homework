class UserCard {
    constructor(name, username, email, id) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.id = id;
        this.posts = [];
    }

    addPost(post) {
        this.posts.push(post)
    }

    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) {
            element.textContent = text;
        }
        element.classList.add(...classNames);
        return element
    }

    renderHeader() {
        const header = this.createElement("div", ["card-header"])
        header.insertAdjacentHTML("afterbegin", `<a href="mailto:${this.email}">${this.name}</a><br><span>(@${this.username})</span>`);
        return header
    }

    renderBody() {
        const body = this.createElement("div", ["card-body"]);
        this.posts.forEach(post => body.append(post.render()));
        return body
    }

    render() {
        const userCard = this.createElement("div", ["card", "user-card"])
        userCard.append(this.renderHeader(), this.renderBody());
        return userCard
    }
}

export default UserCard


