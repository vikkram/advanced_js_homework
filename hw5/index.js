import UserCard from './userCard.js';
import UserPost from "./userPost.js";

const usersPosts = document.querySelector(".tasks-board");

const response = await fetch(`https://ajax.test-danit.com/api/json/users/`)
const users = await response.json();

for (const user of users) {
    const {name, username, email, id} = user;
    const cardUser = new UserCard(name, username, email, id)

    const response = await fetch(`https://ajax.test-danit.com/api/json/posts`)
    const posts = await response.json();
    posts.forEach(post => {
        const {userId, title, body, id} = post;
        if (cardUser.id === userId) {
            const cardPost = new UserPost(userId, title, body, id)
            cardUser.addPost(cardPost);
        }
    })

    usersPosts.append(cardUser.render());
}

