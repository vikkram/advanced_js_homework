'use strict';

const books = [
    {
        author: 'Скотт Бэккер',
        name: 'Тьма, что приходит прежде',
        price: 70,
    },
    {
        author: 'Скотт Бэккер',
        name: 'Воин-пророк',
    },
    {
        name: 'Тысячекратная мысль',
        price: 70,
    },
    {
        author: 'Скотт Бэккер',
        name: 'Нечестивый Консульт',
        price: 70,
    },
    {
        author: 'Дарья Донцова',
        name: 'Детектив на диете',
        price: 40,
    },
    {
        author: 'Дарья Донцова',
        name: 'Дед Снегур и Морозочка',
    },
];

const root = document.createElement('div')
root.classList.add('root')
document.body.appendChild(root)
const ul = document.createElement('ul')
root.appendChild(ul)

let keysArr = []

for (const book of books) {
    for (const key in book) {
        if (!keysArr.includes(key)) {
            keysArr.push(key)
        }
    }
}

for (const book of books) {
    try {
        for (const key of keysArr) {
            if (book[key] === undefined) {
                throw new Error(`В книзі ${book.name} відсутнє поле ${key}`)
            }
        }
        const li = document.createElement('li');
        li.innerHTML = Object.entries(book)
            .map(([key, value]) => `${key}: ${value}`)
            .join(', ')
        ul.appendChild(li)
    } catch (e) {
        console.log(e)
    }
}

console.log(keysArr)

