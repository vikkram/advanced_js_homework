'use strict';

const divFilms = document.querySelector('.films');

async function render(url) {
    const response = await fetch(url);
    const data = await response.json();
    data.map((item) => {
        const film = document.createElement('ul');
        film.classList.add('films__item');
        film.innerHTML = `
          <h1>${item.name}</h1>
          <h2>Episode number: ${item.episodeId}</h2>
          <p>${item.openingCrawl}</p>
        `;
        divFilms.append(film);
        const urls = item.characters;
        const requests = urls.map((url_1) => fetch(url_1));
        Promise.all(requests).then((responses) => responses.forEach((response_1) => {
            response_1.json().then((data_1) => {
                const character = document.createElement('li');
                character.classList.add('films__character');
                character.style.textAlign = 'left';
                character.style.marginTop = '10px';
                character.innerHTML = `
              <h3>Character's name: ${data_1.name}</h3>
            `;
                film.append(character);
            });
        }));
    });
}
render('https://ajax.test-danit.com/api/swapi/films')


// Promise.all([fetchingFilms, fetchingCharacters]).then((data) => {
//   console.log(data);
// });

// fetchingFilms.then((data) => {
//   data.forEach((film) => {
//     const characters = film.characters.map((url) => fetch(url));
//     console.log(characters);
//   });
// });

// console.log(fetchingFilms);

// for (let i = 0; i < film.characters.length; i++) {
//   const element = film.characters[i];
//   fetch(element).then((res) => res.json()).then((data) => {

// fetchingFilms.then((data) => {
//   data.forEach((film) => {
// const filmItem = document.createElement('ul');
// filmItem.classList.add('films__item');
// filmItem.innerHTML = `<h1>Episode name: ${film.name}</h1> <h2>Episode number: ${film.episodeId}</h2> <i>Desctiption: ${film.openingCrawl}</i>`;
// divFilms.appendChild(filmItem);
// console.log(film.characters);
// for (let i = 0; i < film.characters.length; i++) {
//   const element = film.characters[i];
//   fetch(element).then((res) => res.json()).then((data) => {
//     const charactersItem = document.createElement('li');
//     charactersItem.classList.add('films__characters');
// charactersItem.style.textAlign = 'left';
// charactersItem.style.marginTop = '10px';
//     charactersItem.innerHTML = `Character's name: ${data.name}`;
//     filmItem.appendChild(charactersItem);
//   });
// }
//   });
// });
